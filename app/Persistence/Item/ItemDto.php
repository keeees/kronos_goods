<?php
namespace App\Persistence\Item;


class ItemDto {
	
	public $itemId;
	
	public $itemName;
	
	public $category;
	
	public $recommendedFlg;
	
	public $newAndOldId;
	
	public $image;
	
	public $artist;
	
	public $price;
	
	public $remarks;
	
	public $maxIdentificationNumber;
	
	public $createDate;
	
}
