<?php

namespace App\Persistence\Item;

require_once 'ItemDto.php';

use App\Persistence\Item\ItemDto;
use PDO;

class ItemDao {
	private $db;
	public function __construct($db) {
		$this->db = $db;
	}
	
	/**
	 * 全件取得
	 */
	public function findAllItemDatas(){
		try {
			
			$stmt = $this->db->prepare ( 'select
											ITEM.ITEM_ID,
											ITEM_NAME,
											ARTIST,
											PRICE,
											CATEGORY_NAME,
											REMARKS
										from
											ITEM
										left join
											CATEGORY
										on
											ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID' );
			$stmt->execute ();
			$list = null;
			while ( $row = $stmt->fetch () ) {
				$list [] = $row;
			}
			return $list;
		} catch ( PDOException $e ) {
			throw $e;
		}
	}
	
	public function allItemCounts() {
		try {
			
			$stmt = $this->db->prepare ('select count(*) from item');
			$stmt->execute ();
			$list = null;
			while ( $row = $stmt->fetchColumn () ) {
				$list [] = $row;
			}
			return $list;
		} catch ( PDOException $e ) {
			throw $e;
		}
	
	}
	
	/**
	 * オススメ商品ランダム取得
	 *
	 * @param unknown $loginStatus
	 *        	ログイン状態ステータス（ログイン時：1, 未ログイン時：0）
	 * @return NULL|unknown
	 */
	public function findRecommendedItems($loginStatus) {
		if ($loginStatus == 1) {
			// ログインしている場合
			$stmt = $this->db->prepare ( 'select ITEM_ID, ITEM_NAME from ITEM where RECOMMENDED_FLG = 1 order by rand() limit 5' );
		} elseif ($loginStatus == 0) {
			// ログインしていない場合
			$stmt = $this->db->prepare ( 'select ITEM_ID, ITEM_NAME from ITEM where RECOMMENDED_FLG = 1 order by rand() limit 10' );
		}
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_ASSOC );
		$list = null;
		while ( $row = $stmt->fetch () ) {
			$list [] = $row;
		}
		return $list;
	}
	
	/**
	 * 注文商品一覧取得（最大5件）
	 *
	 * @return NULL|unknown
	 */
	public function findOrderedItems($userid) {
		$stmt = $this->db->prepare('select 
										OD.ITEM_ID,
										ITEM_NAME
									from 
										`ORDER` O 
									left join 
										ORDER_DETAIL OD 
									on 
										O.ORDER_NUMBER = OD.ORDER_NUMBER
									left join
										ITEM I
									on
										I.ITEM_ID = OD.ITEM_ID
									where 
										USER_ID = ? 
									order by 
										ORDER_DATETIME desc 
									limit 5;' );
		$stmt->bindValue ( 1, $userid );
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_ASSOC );
		$list = null;
		while ( $row = $stmt->fetch () ) {
			$list [] = $row;
		}
		return $list;
	}
	
	/**
	 * IDに紐づく商品画像を取得
	 *
	 * @param unknown $itemId        	
	 * @return unknown
	 */
	public function findItemImage($itemId) {
		$stmt = $this->db->prepare ( 'select IMAGE from ITEM where ITEM_ID = ? ' );
		$stmt->bindValue ( 1, $itemId );
		$stmt->execute ();
		$stmt->setFetchMode ( PDO::FETCH_ASSOC );
		
		return $stmt->fetchColumn ();
	}
	
	/**
	 * 商品IDに紐づく商品データの取得
	 */
	public function findItemDatas($itemid) {
		try {
			
			$stmt = $this->db->prepare ( 'select  
											ITEM.ITEM_ID,
											ITEM_NAME,
											PRICE,
											CATEGORY_NAME,
											REMARKS
										from
											ITEM
										left join
											CATEGORY
										on
											ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID
										where
											ITEM.ITEM_ID = ?' );
			$stmt->bindValue ( 1, $itemid );
			$stmt->execute ();
			$list = null;
			while ( $row = $stmt->fetch () ) {
				$list [] = $row;
			}
			return $list;
		} catch ( PDOException $e ) {
			throw $e;
		}
	}
	
	/**
	 * あいまい検索
	 * 
	 * @throws PDOException
	 * @return NULL|unknown
	 */
	public function searchAmbiguous($keyword, $categoryid) {
		$sql = ' select
						ITEM.ITEM_ID,
						ITEM_NAME,
						PRICE,
						ITEM.CATEGORY_ID,
						CATEGORY_NAME,
						REMARKS
					from
						ITEM
					left join
						CATEGORY
					on
						ITEM.CATEGORY_ID = CATEGORY.CATEGORY_ID
					where
						ITEM_NAME like ?';
		try {
			if ($categoryid != null) {
				$sql = $sql." having ITEM.CATEGORY_ID = ?";	
			}
			
			$stmt = $this->db->prepare ( $sql );
			$stmt->bindValue ( 1, $keyword != null ? '%'.$keyword.'%' : '%');
			
			if ($categoryid != null) {
				$stmt->bindValue ( 2, $categoryid);
			}

			$stmt->execute ();
			$stmt->setFetchMode ( PDO::FETCH_ASSOC );
			$list = null;
			while ( $row = $stmt->fetch () ) {
				$list [] = $row;
			}
			return $list;
		} catch ( PDOException $e ) {
			throw $e;
		}
	}
	
	/**
	 * 商品ID検索
	 * 
	 * @param unknown $itemid
	 * @throws PDOException
	 * @return NULL|unknown
	 */
	public function findItemData($itemid) {
		try {
			$stmt = $this->db->prepare ( 'select ITEM_ID, ITEM_NAME, CATEGORY_ID, PRICE, REMARKS from ITEM where ITEM_ID = ? ' );
			$stmt->bindValue ( 1, $itemid );
			$stmt->execute ();
			$stmt->setFetchMode ( PDO::FETCH_ASSOC );
			return $stmt->fetch ();
		} catch ( PDOException $e ) {
			throw $e;
		}
	}	
}