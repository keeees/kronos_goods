<?php

namespace App\Persistence\Rental;


class OrderDetailDto {
	
	public $rentalDetailNumber;
	
	public $rentalNumber;
	
	public $itemid;
	
	public $price;
	
	public $identificationNumber;
	
}
