<?php

namespace App\Persistence\Order;

require_once 'OrderDto.php';

use App\Persistence\Order\OrderDto;
use PDO;

class OrderDao {
	private $db;
	public function __construct($db) {
		$this->db = $db;
	}
	public function getDeliveryTimeName() {
		$stmt = $this->db->prepare ( 'select DELIVERY_TIME_NAME from DELIVERY_TIME' );
		$stmt->execute ();
		$list = null;
		while ( $row = $stmt->fetch () ) {
			$list [] = $row;
		}
		return $list;
	}
		
	/**
	 * 注文データの挿入
	 * 
	 * @param unknown $userid
	 * @param unknown $deliveryDate
	 * @param unknown $deliveryTime
	 * @param unknown $useCouponId
	 * @param unknown $returnDate
	 * @throws PDOException
	 * @return unknown
	 */
	public function putOrderDataInOrderTable($userid, $deliveryDate, $deliveryTime) {
		try{
				$stmt = $this->db->prepare('insert into `ORDER`(USER_ID, DELIVERY_DATE, DELIVERY_TIME_ID) values (?, ?, ?)');
				$stmt->bindValue(1, $userid);
				$stmt->bindValue(2, $deliveryDate);
				$stmt->bindValue(3, $deliveryTime);
			    $stmt->execute();
			    return $this->db->query('SELECT LAST_INSERT_ID() as ORDER_NUMBER from `ORDER`')->fetch();
			    			    
		}catch(PDOException $e){
			throw $e;
		}
	}
	
	/**
	 * 注文詳細データの挿入
	 *
	 * @param unknown $orderNumber
	 * @param unknown $itemid
	 * @param unknown $price
	 * @throws PDOException
	 * @return unknown
	 */
	public function putOrderDataInOrderDetailTable($orderNumber, $itemid, $price) {
		try{
				$stmt = $this->db->prepare('insert into ORDER_DETAIL(ORDER_NUMBER, ITEM_ID, PRICE) values (?, ?, ?)');
				$stmt->bindValue(1, $orderNumber);
				$stmt->bindValue(2, $itemid);
				$stmt->bindValue(3, $price);
				$stmt->execute();
		
		}catch(PDOException $e){
			throw $e;
		}
							
	}	
	
	/**
	 * 注文データの検索
	 *
	 * @param unknown $userid
	 * @throws PDOException
	 * @return unknown
	 */
	public function findOrderedData($userid) {
		try{
			$stmt = $this->db->prepare('select
											I.ITEM_ID,
											O.ORDER_NUMBER,
											ORDER_DETAIL_NUMBER,
											ORDER_DATETIME,
											ITEM_NAME,
											OD.PRICE,
											C.CATEGORY_ID,
											CATEGORY_NAME
										from
											`ORDER` O
										left join
											ORDER_DETAIL OD
										on
											O.ORDER_NUMBER = OD.ORDER_NUMBER
										left join
											ITEM I
										on
											OD.ITEM_ID = I.ITEM_ID
										left join
											CATEGORY C
										on
											C.CATEGORY_ID = I.CATEGORY_ID
										where
											USER_ID = ?');
			$stmt->bindValue(1, $userid);
			$stmt->execute();
			$list = null;
			while ($row = $stmt->fetch()) {
				$list[] = $row;
			}
			return $list;
			
		}catch(PDOException $e){
			throw $e;
		}
		
	}

}