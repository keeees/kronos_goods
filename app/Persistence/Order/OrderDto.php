<?php

namespace App\Persistence\Rental;


class OrderDto {
	
	public $rentalNumber;
	
	public $userId;
	
	public $useCouponId;
	
	public $orderDateTime;
	
	public $returnDate;
	
	public $deliveryDate;
	
	public $deliveryTimeId;
	
	public $rentalStaffId;
	
	public $returnStaffId;
	
	public $statusId;
	
}
