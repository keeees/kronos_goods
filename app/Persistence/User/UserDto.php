<?php

namespace App\Persistence\User;

class UserDto {
	
	public $user_id;
	
	public $password;
	
	public $mail_address;
	
	public $user_name;
	
	public $postal_code;
	
	public $address1;
	
	public $address2;
	
	public $tel;
	
	public $birthday;
	
	public $card_number;
	
	public $card_expiration_date;
	
	public $card_expiration_date_year;
	
	public $card_expiration_date_month;
	
	public $card_security_code;
		
	public $delete_flg;
	
}
