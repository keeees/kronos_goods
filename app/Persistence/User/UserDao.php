<?php
namespace App\Persistence\User;

require_once 'UserDto.php';

use App\Persistence\User\UserDto;
use phpDocumentor\Reflection\Types\Boolean;
use PDO;

class UserDao {
	
	private $db;
	
	public function __construct($db) {
		$this->db = $db;
	}
	
	/**
	 * 全権検索
	 * 
	 * @return NULL|unknown
	 */
	public function findAll() {
		$stmt = $this->db->prepare('select * from USER');
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'UserDto');
		$list = null;
		while ($row = $stmt->fetch()) {
			$list[] = $row;
		}
		return $list;
	}
	
	/**
	 * ログイン
	 * 
	 * @param unknown $id
	 * @param unknown $password
	 * @return Boolean
	 */
	public function findByUserIdAndPassword($id, $password) {
		$stmt = $this->db->prepare('select count(*) as COUNT from USER where USER_ID = ? and PASS = PASSWORD(?)');
		$stmt->bindValue(1, $id);
		$stmt->bindValue(2, $password);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		if ($row = $stmt->fetchColumn()) {
			$list[] = $row;
			if ($list[0] == 1) {
				// ログイン成功
				return true;
			} 
		}
		// ログイン失敗
		return false;
	}
	
	/**
	 * ユーザID検索
	 * 
	 * @param unknown $id
	 * @return unknown
	 */
	public function findByUserId($id) {
		$stmt = $this->db->prepare('select * from USER where USER_ID = ? and DELETE_FLG = 0');
		$stmt->bindValue(1, $id);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_CLASS, 'UserDto');
		return $stmt->fetch();
	}
	
	/**
	 * 招待コード存在チェック用
	 * 
	 * @param unknown $invite_code
	 * @return unknown
	 */
	public function findByInviteCode($invite_code) {
		$stmt = $this->db->prepare('select count(*) as COUNT from USER where INVITE_CODE = ?');
		$stmt->bindValue(1, $invite_code);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		return $stmt->fetchColumn();
	}
	
	/**
	 * 新規登録
	 * 
	 * @param UserDto $user
	 */
	public function create(UserDto $user) {
		$stmt = $this->db->prepare(
				'insert into USER values(?, PASSWORD(?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
		$stmt->bindValue(1, $user->user_id);
		$stmt->bindValue(2, $user->password);
		$stmt->bindValue(3, $user->mail_address);
		$stmt->bindValue(4, $user->user_name);
		$stmt->bindValue(5, $user->postal_code);
		$stmt->bindValue(6, $user->address1);
		$stmt->bindValue(7, $user->address2);
		$stmt->bindValue(8, $user->tel);
		$stmt->bindValue(9, $user->birthday);
		$stmt->bindValue(10, $user->card_number);
		$stmt->bindValue(11, $user->card_expiration_date);
		$stmt->bindValue(12, $user->card_security_code);
		$stmt->bindValue(13, $user->delete_flg);
		$stmt->execute();
	}
	
	/**
	 * 更新
	 * 
	 * @param unknown $user
	 * @param unknown $id
	 */
	public function update(UserDto $user) {		
		$stmt = $this->db->prepare(
				'update USER 
					set 
					PASS = PASSWORD(?),
					MAIL_ADDRESS = ?,
					USER_NAME = ?,
					POSTAL_CODE = ?,
					ADDRESS1 = ?,
					ADDRESS2 = ?,
					TEL = ?,
					BIRTHDAY = ?,
					CARD_NUMBER = ?,
					CARD_EXPIRATION_DATE  = ?,
					CARD_EXPIRATION_DATE = ?
				where
					USER_ID = ?');
		$stmt->bindValue(1, $user->password);
		$stmt->bindValue(2, $user->mail_address);
		$stmt->bindValue(3, $user->user_name);
		$stmt->bindValue(4, $user->postal_code);
		$stmt->bindValue(5, $user->address1);
		$stmt->bindValue(6, $user->address2);
		$stmt->bindValue(7, $user->tel);
		$stmt->bindValue(8, $user->birthday);
		$stmt->bindValue(9, $user->card_number);
		$stmt->bindValue(10, $user->card_expiration_date);
		$stmt->bindValue(11, $user->card_security_code);
		$stmt->bindValue(12, $user->user_id);
		$stmt->execute();
		return $stmt->fetchColumn();
	}
	
	/**
	 * 削除
	 * 
	 * @param unknown $id
	 */
	public function delete($id) {
	}

}