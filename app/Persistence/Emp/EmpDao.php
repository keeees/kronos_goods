<?php

namespace App\Persistence\Emp;

require_once 'EmpDto.php';

use App\Persistence\Emp\EmpDto;
use PDO;

class EmpDao {

    private $db;
    
    public function __construct($db) {
        $this->db = $db;
    }
    
    public function findAll() {
        $stmt = $this->db->prepare('select * from EMP');
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'EmpDto');
        $list = null;
        while ($row = $stmt->fetch()) {
            $list[] = $row;
        }
        return $list;
    }
    
    public function findByPrimarykey($id) {
        $stmt = $this->db->prepare('select * from EMP where EMPNO = ?');
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'EmpDto');
        if ($row = $stmt->fetch()) {
        	$list[] = $row;
        	if ($list[0] == 1) {
        		return true;
        	}
        }
        return false;
    }
    
    public function create($emp) {
    }
    
    public function update($emp, $id) {
    }
    
    public function delete($id) {
    }
}