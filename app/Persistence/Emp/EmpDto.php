<?php

namespace App\Persistence\Emp;


class EmpDto {

    public $empno;
    
    public $ename;
    
    public $job;
    
    public $hiredate;
    
    public $mgr;
    
    public $sal;
    
    public $comm;
    
    public $deptno;

}
