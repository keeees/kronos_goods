<?php
function getDb() {
	$dsn = 'mysql:dbname=mmr_online; host=127.0.0.1; charset=utf8';
	$usr = 'root';
	$passwd = '12345';
	
	$db = new PDO ( $dsn, $usr, $passwd, array (
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_EMULATE_PREPARES => false 
	) );
	return $db;
}