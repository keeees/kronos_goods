<?php

namespace App\Http\Controllers;

require_once 'PDOManager.php';

use Session;
use Request;
use Redirect;
use Illuminate\Http\Request as Request_;
use Validator;
use App\Http\Controllers\Controller;
use App\Persistence\User\UserDao;
use App\Persistence\User\UserDto;
use App\Persistence\Coupon\CouponDao;

/**
 * ユーザコントローラ
 *
 * @author mina_saito
 */
class UserController extends Controller {
	
	/**
	 * バリデーションルール
	 *
	 * @var array
	 */
	public $validateRules = [ 
			'user_id' => 'required|string|max:10',
			'user_name' => 'required|string|max:40',
			'mail_address' => 'required|string|email|max:50',
			'password' => 'required|string|between:8,41',
			'password_confirm' => 'required|string|same:password',
			'postal_code' => 'required|numeric|digits:7',
			'address1' => 'required|string|max:50',
			'address2' => 'required|string|max:50',
			'tel' => 'required|numeric|digits_between:5,11',
			'birthday' => 'required|string'
	];
	
	/**
	 * バリデーションメッセージ
	 *
	 * @var array
	 */
	public $validateMessages = [ 
			"required" => "必須項目です。",
			"required_if" => "必須項目です。",
			"numeric" => "数値で入力してください。",
			"email" => "メールアドレスの形式で入力してください。",
			"user_id.max" => "ユーザIDは10文字以下で入力してください。",
			"user_name.max" => "氏名は40文字以下で入力してください。",
			"mail_address.max" => "メールアドレスは50文字以下で入力してください。",
			"password.between" => "パスワードは8文字以上41文字以下で入力してください。",
			"password_confirm.same" => "パスワードと確認用パスワードが異なります。",
			"postal_code.digits" => "郵便番号は7文字で入力してください。",
			"address1.max" => "住所1は50文字以下で入力してください。",
			"address2.max" => "住所1は50文字以下で入力してください。",
			"tel.digits_between" => "電話番号は5文字から11文字で入力してください。",
			"card_number.max" => "カード番号は16文字以下で入力してください。"
	];
	
	/**
	 * 新規登録画面へ遷移
	 */
	public function toRegistration() {
		$id = Session::get ( 'userid' );
		
		// ユーザIDがセッションにある場合(すでにログイン済みの場合)、マイページへ遷移させる
		if ($id != null) {
			return Redirect::to ( '/toMyPage' );
		}
		
		return Redirect::to ( '/registraion' );
	}
	
	/**
	 * マイページへ遷移
	 *
	 * @return unknown
	 */
	public function toMyPage() {
		$id = Session::get ( 'userid' );
		
		// ユーザIDがセッションにない場合、ログインページへ遷移させる
		if ($id == null) {
			return Redirect::to ( '/login' );
		}
		
		$db = getDb ();

		// ログイン中のユーザ情報を取得
		$userDao = new UserDao ( $db );
		$user = $userDao->findByUserId ( $id );
		$user ["CARD_EXPIRATION_DATE_YEAR"] = substr ( $user ["CARD_EXPIRATION_DATE"], 0, 2 );
		$user ["CARD_EXPIRATION_DATE_MONTH"] = substr ( $user ["CARD_EXPIRATION_DATE"], 2 );
		Session::put ( 'user', $user );
		
		return view ( 'user.mypage' );
	}
	
	/**
	 * 登録処理呼び出し
	 *
	 * @param Request $request        	
	 * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse|unknown
	 */
	public function registrationConfirm(Request_ $request) {
		$data = Request::all ();
		$validator = Validator::make ( $data, $this->validateRules, $this->validateMessages );
		
		// validationエラー 新規登録画面にリダイレクト
		if ($validator->fails ()) {
			return redirect ( 'toRegistration' )->withErrors ( $validator )->withInput ();
		}
		Session::pull ( 'userDto', 'default' );
		
		$userDto = new UserDto ();
		$userDto->user_id = $request->input ( 'user_id' );
		$userDto->password = $request->input ( 'password' );
		$userDto->mail_address = $request->input ( 'mail_address' );
		$userDto->user_name = $request->input ( 'user_name' );
		$userDto->postal_code = $request->input ( 'postal_code' );
		$userDto->address1 = $request->input ( 'address1' );
		$userDto->address2 = $request->input ( 'address2' );
		$userDto->tel = $request->input ( 'tel' );
		$userDto->birthday = $request->input ( 'birthday' );
		$userDto->card_number = $request->input ( 'card_number' );
		$userDto->card_expiration_date_year = $request->input ( 'card_expiration_date_year-year' );
		$userDto->card_expiration_date_month = $request->input ( 'card_expiration_date_month-month' );
		$userDto->card_expiration_date = $userDto->card_expiration_date_year . $userDto->card_expiration_date_month;
		$userDto->card_security_code = $request->input ( 'card_security_code' );
		$userDto->delete_flg = 0;
		
		$db = getDb ();
		$userDao = new UserDao ( $db );
		
		// ユーザID存在チェック
		if ($userDao->findByUserId ( $userDto->user_id ) != null) {
			Session::put ( 'userDto', $userDto );
			return redirect ( 'toRegistration' )->with ( 'errormessage', 'そのユーザIDはすでに存在します。' );
		}
			
		// 新規登録実行
		$userDao->create ( $userDto );
		
		return redirect ( 'login' )->with ( 'message', '会員登録が完了しました。');
	}
	
	/**
	 * ユーザ情報更新
	 * 
	 * @param Request_ $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function updateUserConfirm(Request_ $request) {
		// TODO
		$data = Request::all ();
		$validator = Validator::make ( $data, $this->validateRules, $this->validateMessages );
		
		// validationエラー ユーザ情報更新画面にリダイレクト
		if ($validator->fails ()) {
			return redirect ( 'toMyPage' )->withErrors ( $validator )->withInput ();
		}
		
		Session::pull ( 'userDto', 'default' );
		
		$db = getDb ();
		$userDao = new UserDao ( $db );
		
		$user_id = Session::get ( 'userid' );
		if ($userDao->findByUserId ( $user_id ) == null) {
			// TODO エラー画面に遷移
		}
		
		$userDto = new UserDto ();
		$userDto->user_id = $user_id;
		$userDto->user_name = $request->input ( 'user_name' );
		$userDto->password = $request->input ( 'new_password' );
		$userDto->mail_address = $request->input ( 'mail_address' );
		$userDto->postal_code = $request->input ( 'postal_code' );
		$userDto->address1 = $request->input ( 'address1' );
		$userDto->address2 = $request->input ( 'address2' );
		$userDto->tel = $request->input ( 'tel' );
		$userDto->birthday = $request->input ( 'birthday' );
		$userDto->card_number = $request->input ( 'card_number' );
		$userDto->card_expiration_date_year = $request->input ( 'card_expiration_date_year' );
		$userDto->card_expiration_date_month = $request->input ( 'card_expiration_date_month' );
		$userDto->card_expiration_date = $userDto->card_expiration_date_year . $userDto->card_expiration_date_month;
		$userDto->card_security_code = $request->input ( 'card_security_code' );
		
		// password整合性チェック
		if (! $userDao->findByUserIdAndPassword ( $user_id, $request->input ( 'password' ) )) {
			Session::put ( 'userDto', $userDto );
			return redirect ( 'toMyPage' )->with ( 'errormessage', '現在のパスワードが正しくありません。' );
		}
		
		// 更新処理実行
		if ($userDao->update ( $userDto ) == 1) {
			return redirect ( 'toMyPage' )->with ( 'errormessage', 'エラーが発生しました。' );
		}
		return redirect ( 'toMyPage' )->with ( 'message', 'ユーザ情報を更新しました。' );
		;
	}
	
}
