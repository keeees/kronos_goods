<?php

namespace App\Http\Controllers;

use Session;
use Redirect;

/**
 * ログアウトコントローラ
 *
 * @author mina_saito
 */
class LogoutController extends Controller {
	
	/**
	 * ログアウト実行
	 */
	public function doLogout() {
		Session::flush();
		return Redirect::to('/index')->with('message', 'ログアウトしました。');
	}
}