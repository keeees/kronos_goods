<?php

namespace App\Http\Controllers;

require_once 'PDOManager.php';

use DB;
use View;
use Session;
use Request;
use Redirect;
use Illuminate\Http\Request as Request_;
use App\Http\Controllers\Controller;
use App\Persistence\Item\ItemDao;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

/**
 * 商品検索コントローラ
 *
 * @author okano_natsumi
 */
class SearchItemController extends Controller {
	
	/**
	 * 商品検索結果表示
	 */
	public function showSearchResult() {
		// 商品情報の取得
		$db = getDb ();
		$itemDao = new ItemDao ( $db );
		$itemDatas = $itemDao->findAllItemDatas ();
		$beforeItemId = null;
		
		// 商品IDに紐づく商品画像を取得
		foreach ( $itemDatas as $itemData ) {
			$itemImages [] = $itemDao->findItemImage ( $itemData ['ITEM_ID'] );
		}
		Session::put ( 'itemImages', $itemImages );
		
		// ページング
		$perPage = 10;
		$page = max ( 0, Paginator::resolveCurrentPage () - 1 );
		$sliced = array_slice ( $orderData, $page * $perPage, $perPage );
		$paginator = new LengthAwarePaginator ( $sliced, count ( $orderData ), $perPage, null, [ 
				'page' => $page,
				'path' => Paginator::resolveCurrentPath () 
		] );
		
		return view ( 'search.searchView', [ 
				'orderData' => $paginator 
		] );
	}
	
	/**
	 * 商品詳細表示
	 */
	public function showItemDetail(Request_ $request) {
		$itemid = $request->input ( 'itemid' );
	
		$db = getDb ();
		$itemDao = new ItemDao ( $db );
		$itemDatas = $itemDao->findItemDatas ( $itemid );
		Session::put ( 'productDatas', $itemDatas);
		return view('/search/itemDetailView');
	}
	
	/**
	 * カートに入れる処理
	 */
	public function addToCart(Request_ $request) {

		$itemid = $request->input ( 'itemOfId' );
		$db = getDb ();
		try {
			$itemDao = new ItemDao ( $db );
			$item = $itemDao->findItemData ( $itemid );
			$cartInfo = null;
			if (Session::get ( 'cartInfo' ) != null) {
				$cartInfo = Session::get ( 'cartInfo' );
			}
			$cartInfo [] = $item;
			Session::put ( 'cartInfo', $cartInfo );
			return Redirect::to ( 'index' )->with('message', 'カートに商品を入れました。');
		} catch ( PDOException $e ) {
			$db->rollBack ();
			return Redirect::to ( 'index' );
		}
	}
	
	/**
	 * ジャンル情報を取得
	 *
	 * @param Request_ $request        	
	 */
	public function getGenres(Request_ $request) {
		$db = getDb ();
		$itemDao = new ItemDao ( $db );
		$genres = $itemDao->findGenresByCategoryId ( $_POST ['category_val'] );
		header ( 'Content-Type: application/json' );
		echo json_encode ( $genres );
	}
	
	/**
	 * あいまい検索実行
	 *
	 * @param Request_ $request        	
	 */
	public function searchAmbiguous(Request_ $request) {
		
		$categoryid = $request->input ( 'categoryid' );
		$keyword = $request->input ( 'keyword' );
		
		$db = getDb ();
		$itemDao = new ItemDao ( $db );
		$itemDatas = $itemDao->searchAmbiguous ($keyword, $categoryid);
		
		if ($itemDatas == null) {
			Session::forget ( 'hasResult' );
			return view('search.searchView');
		}
		Session::put ( 'hasResult', true );
				
		// ページング
		$perPage = 10;
		$page = max ( 0, Paginator::resolveCurrentPage () - 1 );
		$sliced = array_slice ( $itemDatas, $page * $perPage, $perPage );
		$paginator = new LengthAwarePaginator ( $sliced, count ( $itemDatas), $perPage, null, [ 
				'page' => $page,
				'path' => Paginator::resolveCurrentPath () 
		] );
		
		$paginator->appends(['categoryid' => $categoryid == null ? '' : $categoryid])->links();
		$paginator->appends(['keyword' => $keyword== null ? '' : $keyword])->links();
		
		
		return View::make('search.searchView', [ 'orderData' => $paginator ]);
	}
}