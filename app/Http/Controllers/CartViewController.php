<?php

namespace App\Http\Controllers;

require_once 'PDOManager.php';

use Session;
use Request;
use Redirect;
use Illuminate\Http\Request as Request_;
use App\Http\Controllers\Controller;
use App\Persistence\Cart\CartDao;

/**
 * カート内確認コントローラ
 *
 * @author okano_natsumi
 */
class CartViewController extends Controller {
	
	/**
	 * カート内確認処理
	 */
	public function show(Request_ $request) {
		return Redirect::to ( '/cartView' );
	}
	
	/**
	 * カート内から商品削除
	 *
	 * @param Request_ $request        	
	 * @throws PDOException
	 * @return unknown
	 */
	public function delete(Request_ $request) {
		$itemId　 = $request->input ( 'itemOfId' );
		
		$cartInfos = Session::get ( 'cartInfo' );
		$count = 0;
		foreach ( $cartInfos as $cartInfo ) {
			if ($cartInfo ['ITEM_ID'] == $request->input ( 'itemOfId' )) {
				unset ( $cartInfos [$count] );
				$cartInfos = array_values ( $cartInfos );
				Session::put ( 'cartInfo', $cartInfos );
				break;
			}
			$count ++;
		}
		// セッションに商品情報が一点も含まれない場合、セッションを破棄する
		if (count($cartInfos) == 0) {
			Session::forget('cartInfo');
		}
		return Redirect::to ( '/show' );
	}
}