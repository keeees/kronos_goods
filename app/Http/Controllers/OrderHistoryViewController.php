<?php

namespace App\Http\Controllers;

require_once 'PDOManager.php';

use DB;
use Session;
use Request;
use Redirect;
use Illuminate\Http\Request as Request_;
use App\Http\Controllers\Controller;
use App\Persistence\Order\OrderDao;

/**
 * 注文履歴コントローラ
 *
 * @author okano_natsumi
 */
class OrderHistoryViewController extends Controller {
	
	/**
	 * 注文履歴表示
	 *
	 */
	public function showOrderHistory(Request_ $request)
	{
		try {
			$db = getDb ();
			$orderDao = new OrderDao( $db );
			$userid = Session::get('userid');
			//USER_IDからORDERテーブルにある注文データを取り出す
			$orderdDatas = $orderDao->findOrderedData($userid);
		} catch(PDOException $e) {
			return Redirect::to('/error');
		}
		
		// 注文履歴が存在しない場合
		if($orderdDatas == null) {
			return Redirect::to('/orderHistoryView');
		}
		
		$beforeOrderNumber = null;
		$orderDatas = null;
		$count = 0;
		$totalPrice = 0;
		foreach ($orderdDatas as $orderData){
			if ($beforeOrderNumber != $orderData['ORDER_NUMBER']) {
				$count = 0;
				$totalPrice = 0;
				$beforeOrderNumber = $orderData['ORDER_NUMBER'];
 			}
 			$totalPrice += $orderData['PRICE'];
 			$orderDatas[$orderData['ORDER_NUMBER']]['totalPrice'] = $totalPrice;
 			$orderDatas[$orderData['ORDER_NUMBER']]['count'] = ++$count;
 			$orderDatas[$orderData['ORDER_NUMBER']][] = $orderData;
		}
		krsort($orderDatas);
		Session::put('orderDatas', $orderDatas);
		return Redirect::to('/orderHistoryView');		
	}
}