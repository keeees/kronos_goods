<?php
namespace App\Http\Controllers;

require_once 'PDOManager.php';

use Session;
use App\Http\Controllers\Controller;
use App\Persistence\Item\ItemDao;

/**
 * コントローラ
 *
 * @author okano_natsumi
 */
class HomeController extends Controller {

	/**
	 * ホーム画面表示
	 */
	public function showIndex()
	{
		$userid = Session::get('userid');
		
		$db = getDb ();
		$itemDao = new ItemDao ( $db );
		$items = $itemDao->findRecommendedItems($userid == null ? 0 : 1);
		if ($userid != null) {
			$rentalItemIds = $itemDao->findOrderedItems($userid);
			Session::put('rentalItemIds', $rentalItemIds);
		}
		Session::put('items', $items);
		return view('index');
	}
	
	/**
	 * 画像取得処理
	 */
 	public function showImage($recommendId)
	{
		$db = getDb ();
		$itemDao = new ItemDao ( $db ); 		
		$image = $itemDao->findItemImage($recommendId);
		return $image;
	}
}