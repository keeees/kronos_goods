<?php

namespace App\Http\Controllers;

require_once 'PDOManager.php';

use Session;
use Request;
use Redirect;
use Illuminate\Http\Request as Request_;
use Validator;
use App\Http\Controllers\Controller;
use App\Persistence\User\UserDao;

/**
 * ログインコントローラ
 *
 * @author mina_saito
 */
class LoginController extends Controller {
	
	/**
	 * バリデーションルール
	 *
	 * @var array
	 */
	public $validateRules = [ 
			'userid' => 'required|string|max:30',
			'password' => 'required|string|between:8,41' 
	];
	
	/**
	 * バリデーションメッセージ
	 * カラムの文字数超過・不足などの場合データベースへの問い合わせをせず、
	 * またセキュリティ対策のため同一メッセージ「メールアドレスとパスワードの組み合わせが間違っています。」の表示にて対応
	 *
	 * @var array
	 */
	public $validateMessages = [ 
			"required" => "必須項目です。",
			"userid.max" => "メールアドレスとパスワードの組み合わせが間違っています。",
			"password.between" => "メールアドレスとパスワードの組み合わせが間違っています。" 
	];
	
	/**
	 * ログイン処理
	 * 
	 * @param Request $request
	 * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse|unknown
	 */
	public function checkUser(Request_ $request) 
	{
		$db = getDb ();
				
		$userid = $request->input ( 'userid' );
		$password = $request->input ( 'password' );
		$userDao = new UserDao ( $db );
		
		if ($userDao->findByUserIdAndPassword($userid, $password)) {
 			Session::put('userid', $userid);
			return Redirect::to('/index')->with('message', 'ログインしました。');
		}
		return redirect ( 'login' )->with('errormessage', 'メールアドレスとパスワードの組み合わせが間違っています。');
	}
}