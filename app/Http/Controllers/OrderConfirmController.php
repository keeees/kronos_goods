<?php

namespace App\Http\Controllers;

require_once 'PDOManager.php';

use DB;
use PDO;
use Session;
use Request;
use Redirect;
use Carbon\Carbon;
use Illuminate\Http\Request as Request_;
use App\Http\Controllers\Controller;
use App\Persistence\Order\OrderDao;

/**
 * 注文確認コントローラ
 *
 * @author okano_natsumi
 */
class OrderConfirmController extends Controller {
	
	/**
	 * 初期処理
	 *
	 */
	public function showOrderConfirm(Request_ $request)
	{
		// お届け日取得
		$dt = new Carbon(Carbon::tomorrow());
		setlocale(LC_ALL, 'ja_JP.UTF-8');
		$deliveryDate1 = $dt->formatLocalized('%Y/ %m/ %d (%a)');
		$deliveryDate2 = $dt->addDay()->formatLocalized('%Y/ %m/ %d (%a)');
		$deliveryDate3 = $dt->addDay()->formatLocalized('%Y/ %m/ %d (%a)');
		$deliveryDate4 = $dt->addDay()->formatLocalized('%Y/ %m/ %d (%a)');
		$deliveryDate5 = $dt->addDay()->formatLocalized('%Y/ %m/ %d (%a)');
		$deliveryDate6 = $dt->addDay()->formatLocalized('%Y/ %m/ %d (%a)');
		$deliveryDate7 = $dt->addDay()->formatLocalized('%Y/ %m/ %d (%a)');	
		Session::put('deliveryDate1', $deliveryDate1);
		Session::put('deliveryDate2', $deliveryDate2);
		Session::put('deliveryDate3', $deliveryDate3);
		Session::put('deliveryDate4', $deliveryDate4);
		Session::put('deliveryDate5', $deliveryDate5);
		Session::put('deliveryDate6', $deliveryDate6);
		Session::put('deliveryDate7', $deliveryDate7);
		
		// 受取希望時間取得
		$db = getDb ();
		$orderDao = new OrderDao ( $db );;
		$getDeliveryTimeName= $orderDao->getDeliveryTimeName();
		Session::put('getDeliveryTimeName', $getDeliveryTimeName);
				
		return Redirect::to('/orderConfirm');	
	}
	
	/**
	 * 注文確定処理
	 * 
	 * @param Request_ $request
	 * @return unknown
	 */
	public function order(Request_ $request)
	{		
		// 注文データ取得
		$userid = Session::get('userid');
		$items = Session::get('cartInfo');
		$deliveryDate= $request->input ( 'deliveryDate' );
		$deliveryTime= $request->input ( 'deliveryTime' );
	
		// お届け希望時間をDBに挿入する形に変換
		$setDate = strstr($deliveryDate,' (', true);
		$replacedDate = str_replace("/ ", "", $setDate);
		
		// 注文データをDBに挿入
		try {	
				$db = getDb ();
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$db->beginTransaction();
				$orderDao = new OrderDao ( $db );
							
 				$orderNumber = $orderDao->putOrderDataInOrderTable($userid, $replacedDate, $deliveryTime);
 				foreach($items as $item) {
 					$orderDao->putOrderDataInOrderDetailTable($orderNumber['ORDER_NUMBER'], $item['ITEM_ID'], $item['PRICE']);
 				}

 				// セッションから対象のセッション情報を削除
 				Session::forget('cartInfo');
 				
 				// トランザクションをコミット
 				$db->commit();
				return Redirect::to('orderCompleted');			

		}catch(PDOException $e){
				// 例外発生時トランザクションをロールバック
				$db->rollBack();
				return Redirect::to('index2');
		}
	}
	
}