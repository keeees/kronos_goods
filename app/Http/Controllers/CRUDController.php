<?php
namespace App\Http\Controllers;


require_once 'PDOManager.php';

use App\Http\Controllers\Controller;
use App\Persistence\Emp\EmpDao;
use Illuminate\Http\Request;

class CRUDController extends Controller
{
    public function index()
    {
        // DB接続
        $db = getDb();
        $dao = new EmpDao($db);
        $emps = $dao->findAll();
        if (empty($emps)) {
            $emps = [];
        }
        return view('crud.search', ['q' => '', 'emps' => $emps]);
    }

    public function search(Request $request)
    {
        $q = $request->input('q');
        
        $db = getDb();
        $dao = new EmpDao($db);
        $emps[] = $dao->findByPrimarykey($q);
        if (empty($emps[0])) {
            $emps = [];
        }
        return view('crud.search', ['q' => $q, 'emps' => $emps]);
    }

}
