$('#change').on('click', function() {
	$('.display').toggle(false);
	$('.user').toggle(true);
	$('#change').toggle(false);
	$('.user-info').after('<div class="row"><div class="col-md-4"></div><input type="submit" class="btn btn-warning btn-sm col-md-4 submit" value="変更を保存"><div class="col-md-4"></div>');
}); 
$('#edit').on('click', function() {
	$('.display').toggle(true);
	$('.user').toggle(false);
	$('#change').toggle(true);	
	$('.submit').toggle(false);
});