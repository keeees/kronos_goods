$(function(){
	$(".category-select li a").on('click', function () {
		$(this).parents('.category-group').find('.dropdown-toggle').html($(this).text() + ' <span class="caret"></span>');
		$(this).parents('.category-group').find('input[name="categoryid"]').val($(this).attr("data-value"));
	});
	
	var pair = location.search.substring(1).split('&');
	var arg = new Object;
	for(var i = 0; pair[i]; i++) {
	    var kv = pair[i].split('=');
	    arg[kv[0]] = kv[1];
	}
	if (arg.keyword != null) {
    	$('.keyword').val(decodeURI(arg.keyword));
    }
    if (arg.categoryid != null) {
    	var category = $('ul li a[data-value=' + arg.categoryid + ']').text();
    	$('.dropdown-toggle').html(category + '<span class="caret"></span>');
    	$('.categoryid').val(arg.categoryid);
    }  
})
	
