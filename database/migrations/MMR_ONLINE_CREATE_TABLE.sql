DROP DATABASE MMR_ONLINE;
CREATE DATABASE MMR_ONLINE;
use MMR_ONLINE;
-- テーブルとその項目の挿入

-- お届け時間帯
create table DELIVERY_TIME (
  DELIVERY_TIME_ID CHAR(1) comment '時間帯ID'
  , DELIVERY_TIME_NAME VARCHAR(30) comment '時間帯'
  , constraint DELIVERY_TIME_PKC primary key (DELIVERY_TIME_ID)
) comment 'お届け時間帯' ;

-- 注文詳細
create table ORDER_DETAIL (
  ORDER_DETAIL_NUMBER INT not null auto_increment comment '注文詳細No'
  , ORDER_NUMBER INT comment '注文No'
  , ITEM_ID INT comment '商品ID'
  , PRICE INT comment '価格'
  , constraint ORDER_DETAIL_PKC primary key (ORDER_DETAIL_NUMBER)
) comment '注文詳細' ;

-- 注文
create table `ORDER` (
  ORDER_NUMBER INT not null auto_increment comment '注文No'
  , USER_ID INT comment 'ユーザID'
  , ORDER_DATETIME TIMESTAMP NOT NULL comment '注文日時'
  , DELIVERY_DATE DATE comment 'お届け日'
  , DELIVERY_TIME_ID CHAR(1) comment '時間帯ID'
  , constraint ORDER_PKC primary key (ORDER_NUMBER)
) comment '注文' ;

-- カテゴリ
create table CATEGORY (
  CATEGORY_ID INT not null comment 'カテゴリID'
  , CATEGORY_NAME VARCHAR(10) comment 'カテゴリ'
  , constraint CATEGORY_PKC primary key (CATEGORY_ID)
) comment 'カテゴリ' ;

-- 商品
create table ITEM (
  ITEM_ID INT not null auto_increment comment '商品ID'
  , ITEM_NAME VARCHAR(100) comment '商品名'
  , CATEGORY_ID INT comment 'カテゴリID'
  , RECOMMENDED_FLG TINYINT comment 'おススメ'
  , NEW_AND_OLD_ID CHAR(1) comment '新旧区分ID'
  , IMAGE MEDIUMBLOB comment '商品画像'
  , PRICE INT comment '値段'
  , REMARKS TEXT comment 'その他'
  , CREATE_DATE TIMESTAMP comment '登録日時'
  , constraint ITEM_PKC primary key (ITEM_ID)
) comment '商品' ;

-- ユーザ
create table USER (
  USER_ID int not null auto_increment comment 'ユーザID'
  , PASS VARCHAR(41) comment 'パスワード'
  , MAIL_ADDRESS VARCHAR(50) comment 'メールアドレス'
  , USER_NAME VARCHAR(40) comment '氏名'
  , POSTAL_CODE VARCHAR(7) comment '郵便番号'
  , ADDRESS1 VARCHAR(50) comment '住所１'
  , ADDRESS2 VARCHAR(50) comment '住所２'
  , TEL VARCHAR(11) comment '電話番号'
  , BIRTHDAY DATE comment '生年月日'
  , CARD_NUMBER VARCHAR(20) comment 'カード番号'
  , CARD_EXPIRATION_DATE VARCHAR(4) comment 'カード有効期限'
  , CARD_SECURITY_CODE VARCHAR(4) comment 'カードセキュリティコード'
  , DELETE_FLG TINYINT comment '削除フラグ'
  , constraint USER_PKC primary key (USER_ID)
) comment 'ユーザ' ;

-- 外部キー制約の追加
alter table ITEM
  add constraint ITEM_FK2 foreign key (CATEGORY_ID) references CATEGORY(CATEGORY_ID);
  
alter table `ORDER`
  add constraint RENTAL_FK1 foreign key (DELIVERY_TIME_ID) references DELIVERY_TIME(DELIVERY_TIME_ID);

alter table `ORDER`
  add constraint RENTAL_FK2 foreign key (USER_ID) references USER(USER_ID);
  
alter table ORDER_DETAIL
  add constraint RENTAL_DETAIL_FK1 foreign key (ORDER_NUMBER) references `ORDER`(ORDER_NUMBER);
  
alter table ORDER_DETAIL
  add constraint ORDER_DETAIL_FK2 foreign key (ITEM_ID) references ITEM(ITEM_ID);

-- insert sample user 

select * from USER;
insert into USER values (1, PASSWORD('melon'), 'melon@example.co.jp', '織田信長', '4570056', '愛知県名古屋市南区元星崎町', '字大道３９２－１シティコープ本星崎６０１', '08016191575', '1993-10-24', '1234234534564567', '0528', '4323', 0);
insert into USER values (2, PASSWORD('apple'), 'apple@example.co.jp', '織田ノブなり', '4570056', '愛知県名古屋市南区元星崎町', '字大道３９２－１シティコープ本星崎６０１', '08016191575', '1993-10-24', '1234234534564567', '0528', '4323', 0);
insert into USER values (3, PASSWORD('banana'), 'banana@example.co.jp', '織田', '4570056', '愛知県名古屋市南区元星崎町', '字大道３９２－１シティコープ本星崎６０１', '08016191575', '1993-10-24', '1234234534564567', '0528', '4323', 0);
insert into USER values (4, PASSWORD('orange'), 'orange@example.co.jp', '織田信繁', '4570056', '愛知県名古屋市南区元星崎町', '字大道３９２－１シティコープ本星崎６０１', '08016191575', '1993-10-24', '1234234534564567', '0528', '4323', 0);
insert into USER values (5, PASSWORD('cute'), 'cute@example.co.jp', '織田ノブなりー', '4570056', '愛知県名古屋市南区元星崎町', '字大道３９２－１', '08016191575', '1993-10-24', '1234234534564567', '0528', '4323', 0);
insert into USER values (6, PASSWORD('hello'), 'grape@example.co.jp', '香川輝幸', '1289891', '愛知県名古屋市南区元星崎町', '字大道３２４－２', '08016191575', '1993-10-24', '1234234534564567', '0528', '4323', 0);

-- insert sample delivery_time

select * from delivery_time;
insert into DELIVERY_TIME values (1, '午前中');
insert into DELIVERY_TIME values (2, '１２：００～１５：００');
insert into DELIVERY_TIME values (3, '１５：００～１８：００');
insert into DELIVERY_TIME values (4, '１８：００～２１：００');

-- insert sample category

select * from category;
insert into CATEGORY values (1, '衣類');
insert into CATEGORY values (2, '文房具');
insert into CATEGORY values (3, '生活雑貨');
insert into CATEGORY values (4, '雑貨その他');
insert into CATEGORY values (5, 'その他');

-- sample item

select * from item;

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('社員証レプリカ', 4, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\badge1.JPG'), 1000, 'ファン待望の株式会社クロノス社員証レプリカ！本物そっくり！', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('缶バッジ（灰）', 4, 0, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\badge2.JPG'), 300, 'クロノスのロゴがあしらわれたおしゃれな缶バッジ。カラーは灰色。ビッグサイズ直径5cm。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('缶バッジ（赤）', 4, 0, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\badge3.JPG'), 100, 'クロノスのロゴがあしらわれたおしゃれな缶バッジ。カラーは赤。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('缶バッジ（黄色）', 4, 0, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\badge4.JPG'), 100, 'クロノスのロゴがあしらわれたおしゃれな缶バッジ。カラーは黄色。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('缶バッジ（緑）', 4, 0, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\badge5.JPG'), 100, 'クロノスのロゴがあしらわれたおしゃれな缶バッジ。カラーは緑。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('缶バッジ（2014）', 4, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\badge2014.JPG'), 10000, 'KronosParty2014 で限定配布された幻の缶バッジ！！', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('ファイル', 2, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\file.JPG'), 400, 'スタイリッシュなクロノスファイル。10枚1セットでの販売です。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('型押し本皮キーホルダー', 3, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\keyholder.JPG'), 4000, '本皮にクロノスロゴを型押しした高級感あふれるキーホルダー。数量限定でのご提供です。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('マグカップ（2015）', 3, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\mag2015.JPG'), 2000, 'KronosParty2015 で限定配布されたクリスマスデザインのクロノスマグカップ。デザイナーMASAKIプレゼンツシリーズ。ITに関連するおなじみのキャラクターを見つけられるかな？', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('万年筆', 2, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\pen2016.JPG'), 2000, 'KronosParty2016 で限定配布されたカジュアルな万年筆。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('パーカー（2015）', 1, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\parker2015.JPG'), 4000, 'KronosParty2015 で限定配布された最高級素材を使用したパーカー。デザイナーMASAKIプレゼンツシリーズ。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('ポロシャツ（2015）', 1, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\polo2015.JPG'), 4000, 'KronosParty2015 で限定配布された最高級素材を使用したポロシャツ。デザイナーMASAKIプレゼンツシリーズ。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('トートバッグ（2015）', 3, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\tote2015.JPG'), 5000, 'KronosParty2015 で限定配布された大容量キャンバス地トートバッグ。デザイナーMASAKIプレゼンツシリーズ。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('クロノス棒（2015）', 4, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\stick.JPG'), 500, 'KronosParty2015 で限定配布された美味しい棒。IT業界に精通したキャラクターが集う。デザイナーMASAKIプレゼンツシリーズ。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('クロノスアワード プラチナ レプリカメダル（2015）', 5, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\platinum.JPG'), 50000, 'KronosAward2015 でO氏が受賞したKronosAwardPlatinum賞の幻の記念メダルレプリカ。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('トートバッグ（2012）', 3, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\tote2012.JPG'), 5000, 'KronosParty2012 で限定配布された綿100%トートバッグ。デザイナーHIROSHIプレゼンツ。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('トートバッグ（2010）', 3, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\tote2010.JPG'), 5000, 'KronosParty2010 で限定配布された綿100%トートバッグ。デザイナーHIROSHIプレゼンツ。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('トートバッグ（2016）', 3, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\tote2016.JPG'), 5000, 'KronosParty2016 で限定配布された綿100%トートバッグ。デザイナーHIROMIプレゼンツシリーズ。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('スポーツ用Tシャツ（2016）', 1, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\tshirt2016.JPG'), 7000, 'KronosParty2016 で限定配布された速乾性に優れたスポーツ用Tシャツ。デザイナーHIROMIプレゼンツシリーズ。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('型押しフォトスタンド', 3, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\stand.JPG'), 900, 'クロノス型押しがおしゃれなフォトスタンド。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('アクリルロゴ「K」', 4, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\k.JPG'), 900, 'クロノスの「K」！！！！全種類集めて飾りたい！アクリル製。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('アクリルロゴ「r」', 4, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\r.JPG'), 900, 'クロノスの「r」！！！！全種類集めて飾りたい！アクリル製。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('アクリルロゴ「n」', 4, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\n.JPG'), 900, 'クロノスの「n」！！！！全種類集めて飾りたい！アクリル製。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('アクリルロゴ「o」', 4, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\o.JPG'), 900, 'クロノスの「o」！！！！全種類集めて飾りたい！アクリル製。', current_timestamp());

insert into ITEM (ITEM_NAME ,CATEGORY_ID ,RECOMMENDED_FLG ,IMAGE ,PRICE ,REMARKS ,CREATE_DATE)
values('アクリルロゴ「s」', 4, 1, LOAD_FILE('C:\\ProgramData\\MySQL\\MySQL Server 5.7\\Uploads\\KronosGoods\\s.JPG'), 900, 'クロノスの「s」！！！！全種類集めて飾りたい！アクリル製。', current_timestamp());


