<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
 			<a class="navbar-brand" href="index"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Kronos Goods</a>
		</div>
		
		<!-- 検索エリア -->
		<div class="search-area">
			<form action="searchAmbiguous" method="get" class="navbar-form navbar-left input_form">
				<div class="btn-group category-group">
          			<button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
							カテゴリ
            			<span class="fa fa-caret-down"></span>
          			</button>
					<ul class="dropdown-menu category-select" role="menu" id="category-select">
						<li><a href="javascript:void(0)" data-value="1" class="category">衣類</a></li>
						<li><a href="javascript:void(0)" data-value="2" class="category">文房具</a></li>
						<li><a href="javascript:void(0)" data-value="3" class="category">生活雑貨</a></li>
						<li><a href="javascript:void(0)" data-value="4" class="category">雑貨その他</a></li>
						<li><a href="javascript:void(0)" data-value="5" class="category">その他</a></li>
					</ul>
					<input type="hidden" class="categoryid" name="categoryid" value="">
				</div>				
				<div class="form-group">
					<input type="text" name="keyword" class="form-control input-sm keyword" placeholder="商品検索" value="{{ session('keyword') }}">
				</div>
				<button type="submit" id="search" class="btn btn-success btn-sm">
					<i class="fa fa-search" aria-hidden="true"></i> 検索
				</button>
			</form>
		
		<!-- メニュー -->
		<span class="input_form pull-right">
		@if(session('cartInfo') != null)
			<a class="btn btn-warning btn-sm" href="show"> <i
					class="fa fa-shopping-cart fa-1x" aria-hidden="true"> カート</i>
			</a>
		@endif
		@if(session('userid') != null)
			<a class="btn btn-default btn-sm" href="toMyPage">
				<i class="fa fa-user" aria-hidden="true">マイページ</i>
			</a>
		@endif 
		@if(session('userid') != null)
			<a class="btn btn-danger btn-sm" href="logout"><i
					class="fa fa-sign-out fa-1x" aria-hidden="true">ログアウト</i>
			</a>
		@endif
		</span>
	
		<!-- ログインエリア -->
		</div>
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<div class="input-area pull-right">
				<form action="checkUser" method="post"
					class="navbar-form navbar-left input_form">
					@if(session('userid') == null)
					<div class="form-group">
						<input type="text" name="userid" class="form-control input-sm"
							placeholder="ユーザID"> <input type="password" name="password"
							class="form-control input-sm" placeholder="パスワード">
					</div>
					<button type="submit" id="login" class="btn btn-info btn-sm"><i class="fa fa-sign-in" aria-hidden="true"></i> ログイン</button>
					@endif 
					@if(session('userid') == null) 
						<a href="/toRegistration"
						class="btn btn-warning btn-sm"><i class="fa fa-plus" aria-hidden="true"> 新規会員登録</i> </a> 
					@endif
				</form>
			</div>
		</div>	
	</div>
</nav>