<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- 共通ヘッダ読み込み -->
@include('common.header')

<title>システムエラー</title>

</head>
<body>
<!-- ナビゲーションバー読み込み -->
@include('common.nav')	

<div class="container">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<h2>システムエラーが発生しました。</h2>
		<label>お手数ですが、初めからやり直してください。</label>
	</div>
	<div class="col-md-2"></div>	
</div>

<!-- 共通フッダ読み込み -->
@include('common.footer')
<script type="text/javascript" src="{{asset('/js/rental/cartView.js')}}" ></script>
</body>
</html>