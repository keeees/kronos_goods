<hr>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<label>copyright © 2017 Kronos Goods Co., Ltd All rights reserved.</label>
	</div>
	<div class="col-md-1"></div>
</div>
<!-- jQuery -->
<script src="{{asset('/assets/js/jquery-3.2.1.min.js')}}"></script>

<!-- Bootstrap -->
<script src="{{asset('/assets/js/bootstrap.min.js')}}"></script>

<script type="text/javascript" src="{{asset('/js/common/common.js')}}" ></script>

