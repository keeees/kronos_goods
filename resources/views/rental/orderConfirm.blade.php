<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- 共通ヘッダ読み込み -->
@include('common.header')

<!-- レンタルページ用css -->
<link href="{{asset('/css/rental/rental.css')}}" rel="stylesheet">
<title>注文日時確認</title>

</head>
<body>
	<!-- ナビゲーションバー読み込み -->
	@include('common.nav')
	<div class="container">
		<div class="col-md-2"></div>
		<div class="col-md-8 row">
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<h3><i class="fa fa-check" aria-hidden="true"></i>注文日時確認</h3>
				</div>
				<div class="col-md-1"></div>
			</div>
				<form class="form-horizontal" action="order" method="post">
					<fieldset>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<label for="select">お届け日</label>
								</div>
								<div class="col-md-2"></div>
							</div>
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<select class="form-control col-md-10" id="select" name="deliveryDate">
										<option>{{ session('deliveryDate1') }}</option>
										<option>{{ session('deliveryDate2') }}</option>
										<option>{{ session('deliveryDate3') }}</option>
										<option>{{ session('deliveryDate4') }}</option>
										<option>{{ session('deliveryDate5') }}</option>
										<option>{{ session('deliveryDate6') }}</option>
										<option>{{ session('deliveryDate7') }}</option>
									</select>
								</div>
								<div class="col-md-2"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<label class="heading">受取希望時間帯</label>
								</div>
								<div class="col-md-2"></div>
							</div>
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<div class="panel panel-default">
										<div class="panel-body">
			      							<?php $n=0 ?>
			      							@foreach (Session::get('getDeliveryTimeName') as $deliveryTime)
												<p> <input type="radio" name="deliveryTime"
													value="<?php echo ++$n ?>"> {{ $deliveryTime[0] }}
												</p>
											@endforeach
										</div>
									</div>
								</div>
								<div class="col-md-2"></div>
							</div>
						</div>
					</fieldset>
					<div class="row">
							<div class="col-md-4"></div>
								<button type="submit" id="orderConfirm" class="btn btn-success btn-sm submit col-md-4">注文を確定する</button>
							<div class="col-md-4"></div>
						</div>
				</form>
			</div>
			<div class="col-md-2"></div>
		</div>

	<!-- 共通フッダ読み込み -->
	@include('common.footer')
	<script type="text/javascript" src="{{asset('/js/rental/cartView.js')}}"></script>

</body>
</html>
