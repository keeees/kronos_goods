<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- 共通ヘッダ読み込み -->
@include('common.header')

<!-- レンタルページ用css -->
<link href="{{asset('/css/rental/rental.css')}}" rel="stylesheet">
<title>注文履歴</title>

</head>
<body>
<!-- ナビゲーションバー読み込み -->
@include('common.nav')	

<div class="container">
<div class="col-md-2"></div>
@if(session('orderDatas') != null)
	@foreach (Session::get('orderDatas') as $orderData)
	<table class="table table-striped table-hover table-bordered">
	  <thead>
	    <tr class="success">
	    <form action="OrderHistoryViewController" method="post">
	    	<input type="hidden" name="rentalNumber" value="">
	    </form>	
	      <th>注文日時：{{ $orderData[0]['ORDER_DATETIME'] }}</th>
	      <th>数量：{{ $orderData['count'] }}</th>
	      <th>合計：{{ $orderData['totalPrice'] }}円</th>
	    </tr>
	  </thead>
	  <?php 
	  $count = 1;
	  ?>
	
		<table class="table table-striped table-hover table-bordered">
		  <tbody>
		    @foreach ($orderData as $orderItemData)
		    	@if($count == 1 || $count == 2)
		    		<?php $count++ ?>
					@continue
		    	@endif
			    <tr class="active row">
			      <td class="col-md-4"><img src="{{ URL::to('showImage/'.$orderItemData['ITEM_ID']) }}" width="150" height="150"></td>
			      <th class="col-md-2">
					  <p>ジャンル </p>
			      	　　<p>タイトル </p>
			      	　　<p>単価 </p>
			      </th>
			      <td class="col-md-6">
					  <p>{{ $orderItemData['CATEGORY_NAME'] }} </p>
			      	　　<p>{{ $orderItemData['ITEM_NAME'] }}</p>
			      	　　<p>{{ $orderItemData['PRICE'] }}</p>
			      </td>
			    </tr>
		     @endforeach
		 </tbody>
		</table>
	</table>
	@endforeach
@else
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6"><label>注文履歴はありません。</label></div>
		<div class="col-md-3"></div>
	</div>
@endif
</div>
   
<!-- 共通フッダ読み込み -->
@include('common.footer')
<script type="text/javascript" src="{{asset('/js/rental/cartView.js')}}" ></script>
</body>
</html>