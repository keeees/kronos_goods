<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- 共通ヘッダ読み込み -->
@include('common.header')
<!-- 注文ページ用css -->
<link href="{{asset('/css/order/cartView.css')}}" rel="stylesheet">

<title>ショッピングカート</title>

</head>
<body>
	<!-- ナビゲーションバー読み込み -->
	@include('common.nav')
	<div class="container">
		<div class="col-md-2"></div>
		<div class="col-md-8 row">
			<!-- メッセージ表示エリア -->
			@if (session('message'))
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info">{{ session('message') }}</div>
				</div>
			</div>
			@endif
 			<h3><i class="fa fa-shopping-cart" aria-hidden="true"></i> ショッピングカート</h3>
			<div>
				<div class="center-block row">
					@if(session('userid') != null && session('cartInfo') != null)
					<form action="showOrderConfirm" method="post">
						<button type="submit"
							class="btn btn-success btn-sm col-md-3 pull-right submit id="order"">注文</button>
					</form>
					@elseif(session('cartInfo') != null) 
						<a class="btn btn-success btn-sm col-md-3 pull-right submit" href="/login">ログインして注文</a>
					@else
						<label>現在カートには商品が入っていません。</label>
						<a class="btn btn-success btn-sm col-md-3 pull-right submit" href="/index">TOPへ</a>
					@endif
				</div>
				<div class="center-block row">
					<div class="col-xs-2"></div>
					@if(session('cartInfo') != null)
						<div class="panel panel-primary">
						@foreach(Session::get('cartInfo') as $item)
								<div class="panel-body">
									<div class="col-md-3">
										<img src="{{ URL::to('showImage/'.$item['ITEM_ID'] ) }}"
											width="100" height="100">
									</div>
									<div class="col-md-9 row">
										<label class="col-md-7">{{ $item['ITEM_NAME'] }}</label>
										<label class="col-md-3">{{ $item['PRICE'] }}円</label>
										<div align="right" class="col-md-2">
											<form action="delete" method="post">
												<input type="hidden" value="{{ $item['ITEM_ID'] }}" name="itemOfId"> 
												<input type="submit"
													class="btn btn-danger btn-xs pull-right" id="button" value="削除">
											</form>
										</div>										
									</div>
								</div>
						@endforeach
					@endif
					</div>
					<div class="col-xs-2"></div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
