<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- 共通ヘッダ読み込み -->
@include('common.header')

 <!-- レンタルページ用css -->
<link href="{{asset('/css/rental/rental.css')}}" rel="stylesheet">
<title>注文受付完了</title>

</head>
<body>
<!-- ナビゲーションバー読み込み -->
@include('common.nav')

<div class="container">
<div class="jumbotron">
  <h2 class="mainTitle">ご注文ありがとうございました</h2>
  <p><a href="index" class="btn btn-primary btn-sm ">トップへ</a></p>
  <p><a href="logout" class="btn btn-primary btn-sm ">ログアウト</a></p>
</div>
</div>
	
<!-- 共通フッダ読み込み -->
@include('common.footer')
</body>
</html>