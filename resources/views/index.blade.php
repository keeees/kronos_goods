<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- 共通ヘッダ読み込み -->
@include('common.header')

<!-- indexページ用css -->
<link href="{{asset('/css/index.css')}}" rel="stylesheet">

<title>Kronos雑貨</title>

</head>
<body>
	<!-- ナビゲーションバー読み込み -->
	@include('common.nav')
	<div class="container">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<!-- メッセージ表示エリア -->
			@if (session('message'))
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info">{{ session('message') }}</div>
				</div>
			</div>
			@endif

			<div class="panel panel-success">
				<div class="panel-heading">
					<h4>
						<i class="fa fa-star" aria-hidden="true"></i> オススメ商品
					</h4>
				</div>
				<div class="panel-body row">
					<div class="col-md-1"></div>
					<div class="col-md-10 text-center">
						@foreach(session('items') as $item)
							<div class="items">
								<a href="showItemDetail?itemid={{ $item['ITEM_ID'] }}"> 
									<img src="{{ URL::to('showImage/'.$item['ITEM_ID']) }}" width="100" height="100">
								</a><br>
								<a href="showItemDetail?itemid={{ $item['ITEM_ID'] }}"> 
									{{ $item['ITEM_NAME'] }}
								</a>
							</div>
						@endforeach
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
			@if(session('userid'))
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4>
						<i class="fa fa-clock-o" aria-hidden="true"></i>注文履歴
					</h4>
				</div>
				<div class="panel-body">
				<div class="col-md-1"></div>
					<div class="col-md-10 text-center">
						@if(session('rentalItemIds'))
							@foreach(session('rentalItemIds') as $rentalItemId)
								<div class="items">
									<a href="showItemDetail?itemid={{ $rentalItemId['ITEM_ID'] }}">
										<img src="{{ URL::to('showImage/'.$rentalItemId['ITEM_ID'] ) }}" width="100" height="100">
									</a><br>
									<a href="showItemDetail?itemid={{ $rentalItemId['ITEM_ID'] }}">
										{{ $rentalItemId['ITEM_NAME'] }}
									</a>
								</div>
							@endforeach
						@else
							<label>注文履歴がありません。</label>
						@endif
					</div>
					<div class="col-md-1"></div>
				</div>
			</div>
			@endif
		</div>
	</div>	
	<!-- 共通フッダ読み込み -->
	@include('common.footer')

</body>
</html>