<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- 共通ヘッダ読み込み -->
@include('common.header')

<!-- 新規会員登録ページ用css -->
<title>ログイン</title>
</head>
<body>
	<!-- ナビゲーションバー読み込み -->
	@include('common.nav')

	<div class="container">

		<div class="col-md-2"></div>

		<div class="col-md-8 jumbotron">
			@if (session('message'))
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="alert alert-success">{{ session('message') }}</div>
				</div>
				<div class="col-md-1"></div>
			</div>
			@endif
			<h3 class="mainTitle">ログイン</h3>
			<hr>
			@if (session('errormessage'))
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="alert alert-warning">{{ session('errormessage') }}</div>
				</div>
				<div class="col-md-1"></div>
			</div>
			@endif
			<form action="checkUser" method="post" class="form-horizontal">
				@if($errors->has('userid'))<label class="error">{{
					$errors->first('userid') }}</label> @endif
				@if($errors->has('password'))<label class="error">{{
					$errors->first('password') }}</label> @endif
				<div class="form-group">
					<label class="control-label col-xs-4">ユーザID</label>
					<div class="col-xs-5">
						<input type="text" name="userid" class="form-control" required>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-4">パスワード</label>
					<div class="col-xs-5">
						<input type="password" name="password" class="form-control"
							required>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="col-xs-offset-2 col-xs-10">
						<button type="submit" class="btn btn-info col-xs-10">ログイン</button>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-offset-2 col-xs-10">
						<a href="toRegistration" class="btn btn-warning col-xs-10">新規会員登録</a>
					</div>
				</div>
			</form>
		</div>
	</div>
	
<!-- 共通フッダ読み込み -->
@include('common.footer')
</body>
</html>