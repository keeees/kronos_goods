<?php
$user = Session::get('userDto');
?>

<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
	    <!-- 共通ヘッダ読み込み -->
    	@include('common.header')
    	
        <!-- 新規会員登録ページ用css -->
        <link href="{{asset('/css/user/user.css')}}" rel="stylesheet">
        <title>新規会員登録</title>   	
    </head>
    <body>
    	<!-- ナビゲーションバー読み込み -->
 		@include('common.nav')
 		
	    <div class="container">
	    	
	    	<div class="col-md-2"></div>
	        
	        <div class="col-md-8 jumbotron">
	        <h3 class="mainTitle">新規会員登録</h3>
	        <hr>	  
	        
	        <!-- エラーメッセージ表示エリア -->
	        @if (session('errormessage'))
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-10">
					<div class="alert alert-warning">{{ session('errormessage') }}</div>
				</div>
				<div class="col-md-1"></div>
			</div>
			@endif
			
			<!-- 入力フォーム表示エリア -->
			<form action="registrationConfirm" method="post" class="form-horizontal">
		        <div class="form-group">
		            <label class="control-label col-xs-4">ユーザID <span class="required">*</span></label>
		            <div class="col-xs-5">
		            @if(session('userDto'))
			            <input type="text" name="user_id" class="form-control input-sm" value="{{ $user->user_id }}" required>
			        @else
			        	<input type="text" name="user_id" class="form-control input-sm" required>
			        @endif
			            @if($errors->has('user_id'))<label class="error">{{ $errors->first('user_id') }}</label>@endif
		            </div>
		        </div>
		        <div class="form-group">
		            <label class="control-label col-xs-4">パスワード <span class="required">*</span></label>
		            <div class="col-xs-5">
		            	<input type="password" name="password" class="form-control input-sm" placeholder="8文字以上40字以内" required>
			            @if($errors->has('password'))<label class="error">{{ $errors->first('password') }}</label> @endif
		            </div>
		        </div>
		        <div class="form-group">
		            <label class="control-label col-xs-4">パスワード（確認用） <span class="required">*</span></label>
		            <div class="col-xs-5">
		                <input type="password" name="password_confirm" class="form-control input-sm" placeholder="もう一度入力してください" required>
						@if($errors->has('password_confirm'))<label class="error">{{ $errors->first('password_confirm') }}</label> @endif		            
		            </div>
		        </div>
		        <div class="form-group">
		            <label class="control-label col-xs-4">メールアドレス <span class="required">*</span></label>
		            <div class="col-xs-7">
		                @if(session('userDto'))
							<input type="text" name="mail_address" class="form-control input-sm" value="{{ $user->mail_address }}" required>
			   	        @else
			        		<input type="text" name="mail_address" class="form-control input-sm" required>
			        	@endif
			            @if($errors->has('mail_address'))<label class="error">{{ $errors->first('mail_address') }}</label> @endif		              
		            </div>
		        </div>
		        <div class="form-group">
		            <label class="control-label col-xs-4">氏名 <span class="required">*</span></label>
		            <div class="col-xs-5">
			            @if(session('userDto'))
							<input type="text" name="user_name" class="form-control input-sm" value="{{ $user->user_name }}" required>
			   	        @else
			        		<input type="text" name="user_name" class="form-control input-sm" required>
			        	@endif
			            @if($errors->has('user_name'))<label class="error">{{ $errors->first('user_name') }}</label> @endif
		            </div>
		        </div>       
		        <div class="form-group">
		            <label class="control-label col-xs-4">郵便番号 <span class="required">*</span></label>
		            <div class="col-xs-5">
		                @if(session('userDto'))
							<input type="text" name="postal_code" class="form-control input-sm" value="{{ $user->postal_code }}" maxlength='7' placeholder="例：1638001"　required>
			   	        @else
			        		<input type="text" name="postal_code" class="form-control input-sm" maxlength='7' placeholder="例：1638001" required>
			        	@endif
		                @if($errors->has('postal_code'))<label class="error">{{ $errors->first('postal_code') }}</label> @endif
		            </div>
		        </div>
		        <div class="form-group">
		            <label class="control-label col-xs-4">住所1 <span class="required">*</span></label>
		            <div class="col-xs-7">
		                @if(session('userDto'))
							<input type="text" name="address1" class="form-control input-sm" value="{{ $user->address1 }}" placeholder="例：東京都新宿区西新宿" required>
			   	        @else
			        		<input type="text" name="address1" class="form-control input-sm" placeholder="例：東京都新宿区西新宿" required>
			        	@endif
		                @if($errors->has('address1'))<label class="error">{{ $errors->first('address1') }}</label> @endif
		            </div>
		        </div>
		        <div class="form-group">
		            <label class="control-label col-xs-4">住所2 <span class="required">*</span></label>
		            <div class="col-xs-7">
		            	@if(session('userDto'))
							<input type="text" name="address2" class="form-control input-sm" value="{{ $user->address2 }}" placeholder="例：2-8-1" required>
			   	        @else
			        		<input type="text" name="address2" class="form-control input-sm" placeholder="例：2-8-1" required>
			        	@endif
		                @if($errors->has('address2'))<label class="error">{{ $errors->first('address2') }}</label> @endif
		            </div>
		        </div>
		        <div class="form-group">
		            <label class="control-label col-xs-4">電話番号 <span class="required">*</span></label>
		            <div class="col-xs-5">
		                @if(session('userDto'))
							<input type="text" name="tel" class="form-control input-sm" value="{{ $user->tel }}" maxlength='11' placeholder="例：0353211111" required>
			   	        @else
			        		<input type="text" name="tel" class="form-control input-sm" placeholder="例：0353211111" maxlength='11' required>
			        	@endif
		                @if($errors->has('tel'))<label class="error">{{ $errors->first('tel') }}</label> @endif
		            </div>
		        </div> 		        
		        <div class="form-group">
		            <label class="control-label col-xs-4">生年月日 <span class="required">*</span></label>
		            <div class="col-xs-5">
		                @if(session('userDto'))
							<input type="date" name="birthday" class="form-control input-sm" value="{{ $user->birthday }}" required>
			   	        @else
			        		<input type="date" name="birthday" class="form-control input-sm" required>
			        	@endif
		                @if($errors->has('birthday'))<label class="error">{{ $errors->first('birthday') }}</label> @endif
		            </div>
		        </div>
		        <div class="form-group">
		            <label class="control-label col-xs-4">カード番号&nbsp;&nbsp;&nbsp;</label>
		            <div class="col-xs-5">
		                @if(session('userDto'))
							<input type="text" name="card_number" class="form-control input-sm" value="{{ $user->card_number }}" maxlength='12'>
			   	        @else
			        		<input type="text" name="card_number" class="form-control input-sm" maxlength='12'>
			        	@endif
		                @if($errors->has('$card_number'))<label class="error">{{ $errors->first('$card_number') }}</label> @endif
		            </div>
		        </div>
		        <div class="form-group">
		            <label class="control-label col-xs-4">カード有効期限&nbsp;&nbsp;&nbsp;</label>
 		            <div class="col-xs-2">
 		            	@if(session('userDto'))
							<input type="text" name="card_expiration_date_year" class="form-control input-sm" placeholder="年" value="{{ $user->card_expiration_date_year }}" maxlength='2'>
			   	        @else
							<input type="text" name="card_expiration_date_year" class="form-control input-sm" placeholder="年" maxlength='2'>
			        	@endif
		            </div> 
		            <div class="col-xs-2">
		                @if(session('userDto'))
							<input type="text" name="card_expiration_date_month" class="form-control input-sm" placeholder="月" value="{{ $user->card_expiration_date_month }}" maxlength='2'>
			   	        @else
							<input type="text" name="card_expiration_date_month" class="form-control input-sm" placeholder="年" maxlength='2'>
			        	@endif
		            </div>
		            @if($errors->has('$card_expiration_date_year'))<label class="error">{{ $errors->first('$card_expiration_date_year') }}</label> @endif
		            @if($errors->has('$card_expiration_date_month'))<label class="error">{{ $errors->first('$card_expiration_date_month') }}</label> @endif
		        </div>

		        <div class="form-group">
		            <label class="control-label col-xs-4">セキュリティコード&nbsp;&nbsp;&nbsp;</label>
		            <div class="col-xs-3">
		            	@if(session('userDto'))
							<input type="text" name="card_security_code" class="form-control input-sm" value="{{ $user->card_security_code }}" maxlength='4'>
			   	        @else
			        		<input type="text" name="card_security_code" class="form-control input-sm" maxlength='4'>
			        	@endif
		                @if($errors->has('$card_security_code'))<label class="error">{{ $errors->first('$cardsecuritycode') }}</label> @endif
		            </div>
		        </div>
		        
		        <br>
		        <div class="form-group">
		            <div class="col-xs-offset-2 col-xs-10">
			            <div class="col-xs-2"></div>
		                <button type="submit" class="btn btn-success btn-sm col-xs-6">新規登録</button>
		                <div class="col-xs-2"></div>
		            </div>
		        </div>
		    </form>
		</div>
		
		<div class="col-md-2"></div>
		</div>
		
	<!-- 共通フッダ読み込み -->
	@include('common.footer')
    </body>
</html>
