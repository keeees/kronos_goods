<?php
$user = Session::get ( 'user' );
?>

<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- 共通ヘッダ読み込み -->
@include('common.header')
<!-- マイページ用css読み込み -->
<link href="{{asset('/css/user/mypage.css')}}" rel="stylesheet">

<title>マイページ</title>
</head>
<body>
	<!-- ナビゲーションバー読み込み -->
	@include('common.nav')
	<div class="container">
		<div class="row">
			<!-- エラーメッセージ表示エリア -->
			@if (session('errormessage'))
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<div class="alert alert-warning">{{ session('errormessage') }}</div>
				</div>
				<div class="col-md-2"></div>
			</div>
			@endif

			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<h3 style="display: inline;">{{ $user["USER_NAME"]}}&nbsp;さんのマイページ</h3>
					</div>
				<div class="col-md-2"></div>
			</div>

			<!-- メッセージ表示エリア -->
			@if (session('message'))
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<div class="alert alert-info">{{ session('message') }}</div>
				</div>
				<div class="col-md-2"></div>
			</div>
			@endif

			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<form action="/updateUser" method="post">
						<table class="table table-striped table-hover user-info">
							<thead>
								<tr class="row success">
									<th class="col-md-5">ユーザ情報</th>
									<th class="col-md-7">
										<input type="button" id="change" class="btn btn-primary btn-sm pull-right display" value="変更">
										<input type="button" id="edit" class="btn btn-primary btn-sm pull-right displaynone user" value="変更の取り消し">
									</th>
								</tr>
							</thead>
							<tbody>
								<tr class="row">
									<th class="col-md-5">氏名</th>
									<td class="col-md-7">
										<p class="user_name display">{{ $user["USER_NAME"] }}</p>
										<input type="text" name="user_name" value="{{ $user['USER_NAME'] }}" class="form-control input-sm  displaynone user">
									</td>
								</tr>
								<tr class="row">
									<th class="col-md-5">メールアドレス</th>
									<td class="col-md-7">
										<p class="mail_address display">{{$user["MAIL_ADDRESS"] }}</p>
										<input type="text" name="mail_address" value="{{ $user['MAIL_ADDRESS'] }}" class="form-control input-sm  displaynone user">
									</td>
								</tr>
								<tr class="row">
									<th class="col-md-5">電話番号</th>
									<td class="col-md-7">
										<p class="tel display">{{ $user["TEL"] }}</p>
										<input type="text" name="tel" value="{{ $user['TEL'] }}" class="form-control input-sm  displaynone user">
									</td>
								</tr>
								<tr class="row">
									<th class="col-md-5">住所</th>
									<td class="col-md-7">
										郵便番号&nbsp;:&nbsp;
										<p class="postal_code display inline">{{$user["POSTAL_CODE"] }}</p>
										<input type="text" name="postal_code" value="{{ $user['POSTAL_CODE'] }}" class="form-control input-sm  displaynone user">
										<br> 
										住所1&nbsp;:&nbsp;
										<p class="address1 display inline">{{$user["ADDRESS1"] }}</p>
										<input type="text" name="address1" value="{{ $user['ADDRESS1'] }}" class="form-control input-sm  displaynone user">
										<br>
										住所2&nbsp;:&nbsp;
										<p class="address2 display inline">{{$user["ADDRESS2"] }}</p>
										<input type="text" name="address2" value="{{ $user['ADDRESS2'] }}" class="form-control input-sm  displaynone user">
										<br>
									</td>
								</tr>
								<tr class="row">
									<th class="col-md-5">生年月日</th>
									<td class="col-md-7">
										<p class="birthday display">{{ $user["BIRTHDAY"] }}</p>
										<input type="date" name="birthday" value="{{ $user['BIRTHDAY'] }}" class="form-control input-sm  displaynone user">
									</td>
								</tr>
								<tr class="row">
									<th class="col-md-5">クレジットカード情報</th>
									<td class=" col-md-7">
										@if($user["CARD_NUMBER"] == null)
											<p class="card_setting display">カード情報が設定されていません。</p><br>
										@else
											カード番号&nbsp;:&nbsp;
											<p class="card_number display inline">{{ $user["CARD_NUMBER"] }}</p>
											<input type="text" name="card_number" value="{{ $user['CARD_NUMBER'] }}" class="form-control input-sm displaynone user">
											<br> 
											有効期限&nbsp;:&nbsp;
											<p class="card_expiration_date_year display inline">{{ $user["CARD_EXPIRATION_DATE_YEAR"] }}</p>
											<input type="text" name="card_expiration_date_year" value="{{ $user['CARD_EXPIRATION_DATE_YEAR'] }}" class="form-control input-sm displaynone user">
											年
											<p class="card_expiration_date_month display inline">{{ $user["CARD_EXPIRATION_DATE_MONTH"] }}</p>
											<input type="text" name="card_expiration_date_month" value="{{ $user['CARD_EXPIRATION_DATE_MONTH'] }}" class="form-control input-sm  displaynone user">
											月
											<br>
											セキュリティコード&nbsp;:&nbsp;
											<p class="card_security_code display inline">{{ $user["CARD_SECURITY_CODE"] }}</p>
											<input type="text" name="card_security_code" value="{{ $user['CARD_SECURITY_CODE'] }}" class="form-control input-sm  displaynone user">
											<br>
										@endif
									</td>
								</tr>
								<tr class="row">
									<th class="col-md-5">パスワード</th>
									<td class="col-md-7">
										<p class="password display"></p>
										<input type="password" name="password" value="" placeholder="現在のパスワード" class="form-control input-sm  displaynone user">
										<input type="password" name="new_password" value="" placeholder="新しいパスワード" class="form-control input-sm displaynone user">
										<input type="password" name="new_password_confirm" value="" placeholder="新しいパスワード（確認用）" class="form-control input-sm displaynone user">
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
				<div class="col-md-2"></div>
			</div>

			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<table class="table table-striped table-hover">
						<thead>
							<tr class="row success">
								<th class="col-md-12">注文履歴</th>
							</tr>
						</thead>
						<tbody>
							<tr class="row">
								<td class="col-md-12"><a href="showOrderHistory">注文履歴詳細ページへ</a></td>
							</tr>
						</tbody>
						<tr>
						</tr>
					</table>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>

	<!-- 共通フッダ読み込み -->
	@include('common.footer')
	<!-- mypage用JS -->
	<script type="text/javascript" src="{{asset('/js/user/mypage.js')}}" ></script>
	
</body>
</html>