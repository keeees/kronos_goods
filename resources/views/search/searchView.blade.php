<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- 共通ヘッダ読み込み -->
@include('common.header')

<!-- レンタルページ用css -->
<link href="{{asset('/css/rental/rental.css')}}" rel="stylesheet">
<title>検索結果</title>

</head>
<body>
<!-- ナビゲーションバー読み込み -->
@include('common.nav')	
<div class="container">
	<div class="col-md-2"></div>
		<div class="col-md-8 jumbotron row">
			<h3 class="mainTitle">検索結果</h3>
 				<div class="center-block row">
					<div class="col-xs-2"></div>
					<?php $genles = Session::get('genle'); ?>
					<?php $n = 0?>
					<?php $beforeItemId = null?>
 			@if (session('hasResult') != null)
				@foreach ($orderData as $itemData)
					@if($itemData['ITEM_ID'] != $beforeItemId)
						<div class="panel panel-default">
							<div class="panel-body">						
								<div class="col-md-4">			
	    							<a href="showItemDetail?itemid={{ $itemData['ITEM_ID'] }}"><img class="img-thumbnail" src="{{ URL::to('showImage/'.$itemData['ITEM_ID']) }}" 
	    							width="100" height="100"></a>
	    						</div>
	    						<div class="col-md-8">
	    							<ul class="list-inline">
	    								<li class="list-inline-item"><span class="label label-info">{{ $itemData['CATEGORY_NAME'] }}</span></li>
								<?php $beforeItemId = $itemData['ITEM_ID'];?>    								
	    							</ul>
	    							<label class="text-left"><a href="showItemDetail?itemid={{ $itemData['ITEM_ID'] }}">{{ $itemData['ITEM_NAME'] }}</a></label><br>
									<label class="text-left">
									</label><br>
									<label class="text-left">{{ $itemData['PRICE'] }}円</label>
										<div align="right">
	 										<form action="addToCart" method="post">
												<input type="hidden" value="{{ $itemData['ITEM_ID'] }}" name="itemOfId">
												<input type="submit" class="btn btn-success btn-sm" id="add{{ $itemData['ITEM_ID'] }}" value="カートに入れる">
											</form>
										</div>
										<br>
								</div>
							</div>
						</div>
					@endif
				@endforeach
	 		</div>
	 		<div class="back" align="center">
			 <p>{{ $orderData->render() }}</p>
			</div>
		@else
			<div class="col-md-2"></div>
			<div class="col-md-8 jumbotron row">
				<label>検索結果は0件です。</label>
			</div>
			<div class="col-md-2"></div>
		@endif	
 	</div>
 </div>
 	
<!-- 共通フッダ読み込み -->
@include('common.footer')
<script type="text/javascript" src="{{asset('/js/rental/cartView.js')}}" ></script>
</body>
</html>