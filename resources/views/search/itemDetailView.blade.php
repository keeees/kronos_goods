<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
<!-- 共通ヘッダ読み込み -->
@include('common.header')

<!-- 注文ページ用css -->
<link href="{{asset('/css/rental/rental.css')}}" rel="stylesheet">
<title>商品詳細</title>

</head>
<body>
	<!-- ナビゲーションバー読み込み -->
	@include('common.nav')
	<div class="container">
		<div class="col-md-2"></div>
		<div class="col-md-8 jumbotron row">
				<div class="center-block row">
					<div class="col-xs-2"></div>
						@foreach(Session::get('productDatas') as $productData)
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="col-md-4">
										<img src="{{ URL::to('showImage/'.$productData['ITEM_ID']) }}"
											width="180" height="180">
									</div>
									<div class="col-md-8">
										<ul class="list-inline">
    										<li class="list-inline-item"><span class="label label-info">{{ $productData['CATEGORY_NAME'] }}</span></li>
    									</ul>
										<label class="text-center">{{ $productData['ITEM_NAME'] }}</label><br>
										<label class="text-center">
										</label><br>
										<label class="text-center">{{ $productData['PRICE'] }}円</label><br>
										<div align="center">
 											<form action="addToCart" method="post">
												<input type="hidden" value="{{ $productData['ITEM_ID'] }}" name="itemOfId">
												<input type="submit" class="btn btn-success btn-sm pull-right" id="button" value="カートに入れる">
											</form>
										</div>										
									</div>
									<div class="row">
										<div class="col-md-12">
											<hr>
											{{ $productData['REMARKS'] }}
										</div>
									</div>
								</div>
						@endforeach 
					</div>
					<div class="col-xs-2"></div>
				</div>
				<div class="back">
					<p class="text-center"><button type="button" id="back" class="btn btn-primary  btn-sm" onclick="history.back()">戻る</button></p>	
				</div>
	</div>
</div>
	

	<!-- 共通フッダ読み込み -->
	@include('common.footer')
	<script type="text/javascript"
		src="{{asset('/js/rental/cartView.js')}}"></script>
</body>
</html>
