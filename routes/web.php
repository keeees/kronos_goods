<?php

/**
 * エラーページへ遷移
 */
Route::get('/error', function () {
	return view('/common/error');
});

/** 新規登録 */
Route::get('/toRegistration', function () {
		return view('/user/registration');
});
Route::post('/registrationConfirm', 'UserController@registrationConfirm');
Route::post('/registrationComplete', function() {
	return view('index');
});

/**
 * ユーザ情報更新
 */
Route::post('/updateUser', 'UserController@updateUserConfirm');

/**
 * ログイン
 */
Route::get('/login', function () {
	return view('/login/login');
});
Route::post('/checkUser', 'LoginController@checkUser');

/**
 * マイページに遷移
 */
Route::get('/toMyPage', 'UserController@toMyPage');

/**
 * ログアウト
 */
Route::get('/logout', 'LogoutController@doLogout');

/**
 * ホーム画面
 */
Route::get('/index', 'HomeController@showIndex');
Route::get('/showImage/{recommendId}', 'HomeController@showImage');

/**
 * 商品詳細情報取得
 */
Route::get('/showItemDetail', 'SearchItemController@showItemDetail');

/**
 * 商品検索結果画面
 */
Route::any('/getGenres', 'SearchItemController@getGenres');
Route::any('/searchAmbiguous', 'SearchItemController@searchAmbiguous');
Route::get('/showSearchResult', 'SearchItemController@showSearchResult');
Route::post('/addToCart', 'SearchItemController@addToCart');

 /**
  * カート内確認画面
  */
Route::get('/show', 'CartViewController@show');
Route::post('/delete', 'CartViewController@delete');
Route::get('/cartView', function() {
	return view('/rental/cartView');
});

 /**
  * 注文確認画面
  */
Route::get('/showOrderConfirm', 'OrderConfirmController@showOrderConfirm');	
Route::post('/showOrderConfirm', 'OrderConfirmController@showOrderConfirm');
Route::get('/orderConfirm', function() {
	return view('/rental/orderConfirm');
});

 /**
  * 注文完了画面
  */
Route::post('/order', 'OrderConfirmController@order');
Route::get('/orderCompleted', function() {
	return view('/rental/orderCompleted');
});

/**
 * 注文履歴画面
 */
Route::get('/showOrderHistory', 'OrderHistoryViewController@showOrderHistory');
Route::post('/showOrderHistory', 'OrderHistoryViewController@showOrderHistory');
Route::get('/orderHistoryView', function() {
	return view('/rental/orderHistoryView');
});
